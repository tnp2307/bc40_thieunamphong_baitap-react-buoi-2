import React, { Component } from "react";

export default class RenderGlasses extends Component {
  state = {
    glassList: [
      {
        id: 1,
        price: 30,
        name: "GUCCI G8850U",
        url: "./glassesImage/v1.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 2,
        price: 50,
        name: "GUCCI G8759H",
        url: "./glassesImage/v2.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 3,
        price: 30,
        name: "DIOR D6700HQ",
        url: "./glassesImage/v3.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 4,
        price: 70,
        name: "DIOR D6005U",
        url: "./glassesImage/v4.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 5,
        price: 40,
        name: "PRADA P8750",
        url: "./glassesImage/v5.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 6,
        price: 60,
        name: "PRADA P9700",
        url: "./glassesImage/v6.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 7,
        price: 80,
        name: "FENDI F8750",
        url: "./glassesImage/v7.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 8,
        price: 100,
        name: "FENDI F8500",
        url: "./glassesImage/v8.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 9,
        price: 60,
        name: "FENDI F4300",
        url: "./glassesImage/v9.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
    ],
    id: 1,
    price: 30,
    name: "GUCCI G8850U",
    url: "./glassesImage/v1.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  };

  renderGlassList = () => {
    let glassListComponent = this.state.glassList.slice(1).map((glass) => {
      return (
        <button
          onClick={() =>
            this.handleShowKinh(
              `${glass.url}`,`${glass.price}`,`${glass.name}`,`${glass.desc}`
            )
          }
        >
          <img
            className="img-thumbnail"
            width={100}
            height={100}
            src={glass.url}
            alt
          />
        </button>
      );
    });
    return glassListComponent;
  };
  handleShowKinh = (glassUrl, glassPrice, glassName, glassDesc) => {
    console.log(glassUrl);
    this.setState({
      url: glassUrl,
      price: glassPrice,
      name: glassName,
      desc: glassDesc,
    });
  };
  render() {
    return (
      <div>
        <div>
          <div className="container vglasses py-3">
            <div className="row justify-content-between">
              <div className="col-6 vglasses__left">
                <div className="row">
                  <div className="col-12">
                    <h1 className="mb-2">Virtual Glasses</h1>
                  </div>
                  {this.renderGlassList()}
                </div>
                <div className="row" id="vglassesList" />
              </div>
              <div className="col-5 vglasses__right p-0">
                <div className="vglasses__card">
                  <div className="mb-2 text-right mt-2 mr-2">
                    <button
                      className="btn btn-warning"
                      onclick="removeGlasses(false)"
                    >
                      Before
                    </button>
                    <button
                      className="btn btn-warning"
                      onclick="removeGlasses(true)"
                    >
                      After
                    </button>
                  </div>
                  <div className="vglasses__model" id="avatar">
                    <img
                      id="glassInput"
                      className=""
                      src={this.state.url}
                      alt=""
                    />
                  </div>

                  <div id="glassesInfo" className="vglasses__info ">
                    <div id="title" className="mt-2">
                      <span id="name">{this.state.name}</span>
                    </div>
                    <div id="availability" className="d-flex mt-2">
                      <button className="btn-danger mr-3">
                        <div id="price">{this.state.price}</div>
                      </button>
                      <div id="stock" className="text-success">
                        Stocking
                      </div>
                    </div>
                    <div className="mt-2" id="description">
                      {this.state.desc}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
