import logo from "./logo.svg";
import "./App.css";
import RenderGlasses from "./RenderGlasses/RenderGlasses";

function App() {
  return (
    <div className="App">
      <h1 className="bg-secondary text-light">Try on you class</h1>
      <RenderGlasses/>
    </div>
  );
}

export default App;
