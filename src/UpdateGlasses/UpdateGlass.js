import React, { Component } from 'react'

export default class UpdateGlass extends Component {
    state={
        id: 0,
        price: 0,
        name: "",
        url: "22",
        desc: "",
    }
    handleShowKinh = (glassUrl) => {

        console.log(glassUrl);
    this.setState({
    url:glassUrl,
    })
  };
  render() {
    return (
         <div className="col-5 vglasses__right p-0 ">
                <div className="vglasses__card">
                  <div className="mb-2 text-right mt-2 mr-2">
                    <button
                      className="btn btn-warning"
                      onclick="removeGlasses(false)"
                    >
                      Before
                    </button>
                    <button
                      className="btn btn-warning"
                      onclick="removeGlasses(true)"
                    >
                      After
                    </button>
                  </div>
                  <div className="vglasses__model" id="avatar" >
                  <img
                    id="glassInput"
                    src={this.state.url}
                    alt=""
                  />
                </div>
                 
                  <div id="glassesInfo" className="vglasses__info ">
                    <div id="title" className="mt-2">
                      <span id="name" />
                      <span id="brand" />( <span id="color" />)
                    </div>
                    <div id="availability" className="d-flex mt-2">
                      <button className="btn-danger mr-3">
                        <div id="price" />
                      </button>
                      <div id="stock" className="text-success">
                        Stocking
                      </div>
                    </div>
                    <div className="mt-2" id="description" />
                  </div>
                </div>
              </div>

    )
  }
}
